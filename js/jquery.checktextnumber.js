/**
 * checkTextNumber
 * 计算文本框还可输入多少文字
 * 尾调用办法：$(element).checkTextNumber();
 * http://www.ancelee.com
 */

;(function($) {
	$.fn.checkTextNumber = function(options) {
		var that = this;
		var opts = $.extend($.fn.checkTextNumber.defaults, options);
		$.fn.checkTextNumber.init($(this), opts);
	};

	$.fn.checkTextNumber.init = function($obj, opts) {
		var setNum;
		$obj.on("focus", function() {
			setNum = setInterval(function(){$.fn.checkTextNumber.numChange($obj, opts)}, 200);
		});
		$obj.on("focusout", function() {
			setNum = clearInterval(setNum)
		});
	};

	$.fn.checkTextNumber.numChange = function($obj, opts) {
		var strlen = 0; //初始定义长度为0
		var st;
		var textcon = $.trim($obj.val());
		for (var i = 0; i < textcon.length; i++) {
			if (textcon.charCodeAt(i) > 255) {
				strlen = strlen + 2; //中文为2个字符
			} else {
				strlen = strlen + 1; //英文一个字符
			}
		}
		strlen = Math.ceil(strlen / 2); //中英文相加除2取整数

		if (opts.maxNum - strlen < 0) {
			st = "超出 <b style='color:#f00;' class=" + opts.numClassName + ">" + Math.abs(opts.maxNum - strlen) + "</b> 字"; //超出的样式
		} else {
			st = "还可以输入 <b class=" + opts.numClassName + ">" + (opts.maxNum - strlen) + "</b> 字"; //正常时候
		};

		if (opts.numObj && $(opts.numObj).length > 0) {
			var $numobj = $(opts.numObj);
			$numobj.html(st)
		} else {
			if ($(".textnumbox").length > 0) {
				$(".textnumbox").html(st)
			} else {
				var numHtml = $('<div class="textnumbox"></div>');
				numHtml.html(st);
				$obj.after(numHtml);
			}
		}
	};

	$.fn.checkTextNumber.defaults = {
		maxNum: 140, //可输入的最大字符
		numClassName: "numbox", //显示字数的样式名称
		numObj: null, //绑定显示数字的元素，可以是jquery对象，$(document)
		btnObj: null, //绑定按钮对象，可以是jquery对象，$(document)
	};
})(jQuery);